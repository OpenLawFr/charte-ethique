# Bienvenue !

## objet de la charte

Cette charte a vocation à donner confiance aux différents acteurs (administrations, entreprises, associations et collectifs citoyens) qui souhaitent initier des démarches de participation mobilisant des outils numériques.

##Contexte

Initiée par un collectif citoyen en 2016 et collectivement rédigée avec des acteurs du secteur public et du secteur privé, cette Charte a accompagné l’adoption croissante du numérique par les acteurs publics souhaitant co-construire leur action. Elle répond aujourd’hui à l’usage généralisé de cette pratique, y compris dans le secteur privé.

## Présentation de la structure de la charte

La Charte est composée d’un préambule et de quatres sections visant à retranscrire des idées forces  : Collaboration, Participation, Transparence et Engagements. Texte de référence pour toute entité souhaitant entamer une démarche de participation appuyée sur le numérique, elle fixe un certain nombre d’engagements qui sont tout autant de principes, de valeurs et d’éléments méthodologiques.

## Présentation de la consultation

Les associations Code for France et Open Law* le droit ouvert ont choisi de soumettre le texte rédigé à consultation grâce à l’outil decidim. Cette démarche sert d’une part, à enrichir les contributions et les avis déjà apportés, à visibiliser l’initiative, et d’autre part à permettre aux acteurs intéressés de saisir du texte et de le modeler selon leurs besoins et leurs aspirations. Il s’agit de créer une véritable dynamique participative et de fédérer des acteurs très divers autour de standards communément partagés. 

Lien d’accès de la démarche : à venir

## Suite du programme et calendrier

La consultation sera ouverte à partir du 27 février 2020, jusqu’à la fin du mois de mai. Une table ronde réunissant des acteurs intéressés par la question de participation citoyenne se déroulera au sein des locaux d’Open Law* le droit ouvert se déroulera le jour du lancement et plusieurs moments de rencontres physiques seront prévus lors des mois de consultation. 

Pour en savoir plus… 
