## A PROPOS 

### Pourquoi cette Charte ? 

Cette charte a vocation à **donner confiance aux différents acteurs (administrations, entreprises, associations et collectifs citoyens) qui souhaitent initier des démarches de participation mobilisant des outils numériques**. 

Générique, elle est mobilisable pour tout type de démarche participative, ponctuelle ou permanente (budget participatif, consultation, boîte à idées, etc.). 

Initiée par un collectif citoyen en 2016 et collectivement rédigée avec des acteurs du secteur public et du secteur privé, cette Charte visait initialement à **accompagner la tendance croissante d'acteurs publics à se saisir du numérique pour co-construire leur action**. Elle répond aujourd'hui à **l'usage généralisé de cette pratique**, y compris dans le secteur privé. 

S'il est source d'émancipation, le recours au numérique constitue également une  source de défiance et implique de **mettre en œuvre des dispositifs éthiques et respectueux des enjeux démocratiques**. 

Ce document propose donc un **cadre de confiance** pour accompagner et faciliter les démarches participatives. Il vise ainsi à devenir un texte de référence pour toute entité, en fixant un certain nombre de repères, qui sont tout autant de principes, de valeurs et d’éléments méthodologiques. 

### Synthèse des principaux engagements 

[A remonter en haut du texte de la Charte ?]

**En résumé, de quoi se compose cette Charte ?**

* Un **préambule** pose le contexte et les objectifs de la Charte 

* Des **définitions** précisent les termes employés correspondant aux différents rôles des parties prenantes lors d’une démarche de participation. 

* **Quatre sections** différentes avec des **engagements** : 
    * La **section 1 "COLLABORATION"** vise à la bonne mise en œuvre d’un dispositif collaboratif entre les parties prenantes. Concrètement, cela implique de s’assurer que la démarche entreprise n’est pas déjà existante, que les différentes parties prenantes puissent participer aux éléments structurants de la démarche, et que la gouvernance mise en oeuvre soit partagée.  
    * La **section 2 "PARTICIPATION**" a pour objectif de favoriser l’inclusion des participants tout au long de la démarche. Elle préconise ainsi d'hybrider les temps physiques et numériques de participation, d'expliciter clairement les règles de participation et les outils les accompagnant et précise les modalités liées au traitement des diverses contributions. 

    * La **section 3 "TRANSPARENCE"** veille à garantir un degré maximal de transparence aussi bien en amont de la démarche, que pendant et après celle-ci. Elle préconise que les informations relatives à la démarche soient claires et que les différentes parties soient correctement identifiées et identifiables. Elle précise des engagements pour une utilisation éthique des outils numériques : explicabilité des algorithmes utilisés, open fair data par défaut, recours au logiciel libre et open source.
    * La **section 4 « ENGAGEMENT »** met en avant quelques points de vigilance clés à la charge du responsable de la démarche en matière de sécurité des données, d'hébergement des outils numériques, d’évaluation de la démarche et de suivi des décisions prises. 

### Une consultation ouverte pour finaliser le texte !

[Préciser le contexte de la consultation, qui l'opère etc. pour un billet de blog et pour la plateforme de CFF : il faut qu'on s'applique nous mêmes les principes de la Charte ;-) + qu'on pense à ouvrir une boite à idées pour la rendre digeste, accessible...]

Objectifs de la consultation (cf atelier NEC) : 
* Avoir un retour sur l’intelligibilité du texte : la rédaction est-elle compréhensible ? Accessible indépendemment des compétences de chacun ?
* Avoir un retour sur la pertinence du texte : les différentes sections semblent-elles utiles ? Suffisantes ? 
* Avoir un retour sur la faisabilité des principes et méthodes présentés : opérationnellement, est-ce applicable ? 

## CONTRIBUTEURS 
**Participants :**  Benjamin Jean, Cristel BRISAC DA FONSECA, Caroline Corbal, Cindy Kus, Armony Altinier, Paul Richardet, Jim et Danaé de la CNPD, Myriam Hammad, Fiorella Bourgeois, Tatiana de Feraudy (+ retrouver les autres)
