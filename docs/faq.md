# FAQ

## **Accessibilité : quelles bonnes pratiques pour rendre une Démarche participative accessible ?**

Exemple de bonnes pratiques :
* Toolbox : https://pidila.gitlab.io/developpement/shortlist.html  
* Test accessibilité : https://asqatasun.org/ 

## **Protection des données**

Plusieurs liens :
* Rapport CNIL : https://www.cnil.fr/fr/civic-tech-donnees-et-demos-une-exploration-des-interactions-entre-democratie-et-technologies 
* Recommandation CNIL en matière de design
* Formats ouverts : définir avec Art.4 de la loi n°2004-575, du 21 juin 2004 pour « la confiance dans l’économie numérique »
* Anonymisation : préciser l'article 15 de la Charte, notamment que la question de l'anonymisation a été pensée et demeure possible dans certains contextes ( exemple : consultation en interne dans une entreprise > laisser la possibilité de contribuer de façon anonyme selon le contexte)

## **Sécurité**

Il est nécessaire de :
* Mettre en place une veille 
* Rapport ANSSI "Guide des bonnes pratiques de l'informatique" : https://www.ssi.gouv.fr/uploads/2017/01/guide_cpme_bonnes_pratiques.pdf

## **Licences libres/ouvertes** 

Quelques ressources :
* Définir les licences libres 
* VVL : http://vvlibri.org/fr 
* Recommander une licence Open Data ? 
