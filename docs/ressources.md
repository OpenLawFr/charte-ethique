## RESSOURCES UTILES 
* Commision national du débat public : https://www.debatpublic.fr/ 
* Rapport du COPEIA : http://www.gouvernement.fr/sites/default/files/contenu/piece-jointe/2018/03/coepia_guide_consultations_janvier2018.pdf
* Rapport CNIL : https://www.cnil.fr/fr/civic-tech-donnees-et-demos-une-exploration-des-interactions-entre-democratie-et-technologies 
